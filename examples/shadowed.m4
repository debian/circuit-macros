.PS
gen_init
divert(-1)

divert(0)dnl

Boxes: [
shadebox(box "s{}hadebox",lthick*4/(1bp__))

move
#{print "B"}
B: shadowed(,,
    shadowthick=lthick*4;attrib=fill_(0.95) "box")

move
#{print "B1"}
B1: shadowed(,,
    rad=0bp__; shadowthick=lthick*4; shadowcolor="lightgray"; shadowangle=-60; \
    attrib=fill_(0.85) "box")

move
#{print "R"}
R: shadowed(box,, rad=6bp__; shadowthick=lthick*4; \
    attrib=wid boxht*2 shaded "orange" outlined "red" thick 2 \
    `"\tt box\char44\char44" "\tt rad=6bp\_\_"')
]

Others:[
#{print "C"}
C: shadowed(circle,,
    shadowthick=lthick*4;shadowcolor="blue";\
      attrib=outlined "green" shaded "green" diam 0.75 "circle")

move
#{print "E"}
 E: shadowed(ellipse,,
     shadowthick=lthick*4; attrib=ht boxwid wid boxht shaded "yellow" \
     dashed "ellipse")

move right movewid*3/2 then down moveht/2; right_
 b = boxht
 F: shadowed(line,,
     attrib=left b then up b right b then down b right b then to Here \
       shaded "orange" outlined "red" thick 3;shadowthick=4.5bp__;\
       shadowangle=45)
    "line" at F.n+(0,-0.3)
] with .nw at Boxes.sw+(0,-moveht)

.PE
