.PS
# TR_fill.m4
# https://tex.stackexchange.com/questions/654117/fill-parts-of-a-shape
gen_init

  B: (0,0); "B" at B below
  C: (5,0); "C" at C below
  A: Cintersect(B,4,C,3); "A" at A above
  line from B to C then to A then to B
  RightAngle(B,A,C,0.25)

  rgbfill(0.5,0.5,0.5,
    arc from A to B rad 2
    arc cw to C rad 2.5
    arc to A rad 1.5 )

.PE
