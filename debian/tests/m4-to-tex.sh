#!/bin/sh

set -e

# Try to generate a TeX file from different circuit macros examples.
echo -n "Test 1: Running..."
/usr/bin/m4 /usr/share/circuit-macros/gpic.m4 /usr/share/doc/circuit-macros/examples/Sixpole.m4 | /usr/bin/gpic -t > /dev/null
echo -n "Done."
echo -n "Test 2: Running..."
/usr/bin/m4 /usr/share/circuit-macros/pgf.m4 /usr/share/doc/circuit-macros/examples/Sixpole.m4 | /usr/bin/dpic -g > /dev/null
echo -n "Done."
