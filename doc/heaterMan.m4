.PS
# heaterMan.m4
cct_init

movewid = 2 pt__
hm = 2.05
right_
{
  {lamp ; move; "`{\tt lamp}'" ljust}
  move right_ 1.5
  {lamp(,T) ; move;"`{\tt lamp(,T)}'" ljust}
  move right_ 1.25
  {thermocouple ; move;" `{\tt thermocouple}'" ljust }
  move right_ hm
  {thermocouple(,,,T) ; move; "`{\tt thermocouple(,{,},T)}'" ljust }
  }
move down 0.25; right_
{
  {heater; move;"`{\tt heater}'" ljust}
  move right_ hm
  {heater(,,,,E); move;"`{\tt heater(,{,},{,}E)}'" ljust}
  move right_ hm+0.4
  {heater(,,,,ET); move;"`{\tt heater(,{,},{,}ET)}'" ljust}
  }

.PE
